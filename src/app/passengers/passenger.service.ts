import { Passenger, passengers } from "src/assets/passengers";
import { Observable, of } from 'rxjs';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })

export class PassengerService {
   Passengers=passengers;
   url="http://localhost:3000/passengers";
   httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http:HttpClient) {}

  public getPassengers(): Observable <Passenger[]> {
        return this.http.get<Passenger[]>(this.url,this.httpOptions)
  }
  public addPassenger(passenger:Passenger):Observable<Passenger>{
    passenger={...passenger,checkedIn:!!(Math.floor(Math.random()*2))} // random checkin status
  
    return this.http.post<Passenger>(this.url,passenger,this.httpOptions);
  }
  public editPassenger(passenger:Passenger):Observable<any>{
        const url_=`${this.url}/${passenger.id}`
        return this.http.put(url_,passenger,this.httpOptions)
  }
  public removePassenger(id:number):Observable<any>{
         const url_=`${this.url}/${id}`
         return this.http.delete(url_,this.httpOptions)
                      }
}

     /***********  prev get passengers  ***********/
    //const id=(this.Passengers.length?Math.max(...this.Passengers.map(p=>p.id))+1 : 0);
     // this.Passengers=this.Passengers.concat([passenger])
   // return this.http.put<>(`${this.url}/{${id}}`,p,this.httpOptions)
    //**************** prev edit + remove  */
    //this.Passengers=this.Passengers.map(p=>(p.id==passenger.id)?Object.assign({},p,passenger):p)
             //this.Passengers=this.Passengers.filter(({id:_id})=>_id!==id);

