import { Component, OnInit } from "@angular/core";

import { Passenger } from "src/assets/passengers";
import { PassengerService } from "../passenger.service";

@Component({
  selector: "component-dashboard",
  templateUrl: `./passenger-dashboard.html`,
  styleUrls: ["./passenger-dashboard.component.css"],
})
export class PassengerDashboardComponent implements OnInit {
  public passengers=[];

  constructor(private passengerService: PassengerService) {}

  ngOnInit() {
    //this.passengers =
    this.passengerService.getPassengers().subscribe(p=>{this.passengers=p;console.log(p)});
  }

  editPassenger(passenger: Passenger) {
    this.passengerService.editPassenger(passenger).subscribe()
    this.passengers = this.passengers.map((p) => {
      if (p.id === passenger.id) {
        return Object.assign({}, p, passenger);
      }
      return p;
    });
  }
  addNewPassenger(passenger:Passenger){
    this.passengers=this.passengers.concat([passenger])
  }

  removePassenger(id: number) {
    this.passengerService.removePassenger(id).subscribe();
    this.passengers = this.passengers.filter(
      ({id:_id}) => _id !== id
    );
  }
}
