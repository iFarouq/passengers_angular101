import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Passenger } from "src/assets/passengers";

@Component({
  selector: "passenger-list",
  templateUrl: `./passenger-list.html`,
  styleUrls: ["./passenger-list.component.css"],
})
export class PassengerListComponent {
  @Input() passenger: Passenger;
  @Output() edit: EventEmitter<Passenger> = new EventEmitter();
  @Output() remove: EventEmitter<number> = new EventEmitter();
  editing: boolean = false;
  passengerToEmit: Passenger;

  toggleEdit() {
    if (this.editing) {
      this.edit.emit(this.passengerToEmit);
    }
    this.editing = !this.editing;
  }

  handleFullNameEdit(event: any) {
    if(!event.target.value) return
    this.passengerToEmit = { ...this.passenger, fullName: event.target.value };
  }

  handleRemove(id: number) {
    if(confirm("Are you sure you want to delete this passenger!"))this.remove.emit(id);
  }
}
